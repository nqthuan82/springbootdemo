package com.example.demo.configurations.security;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@ConfigurationProperties(prefix = "auth0")
@Validated
public class OAuthConfigurationProperties {

    @NotNull
    @NotBlank
    private String audience;

    @NotNull
    @NotBlank
    private String domain;

    @NotNull
    @NotBlank
    private String clientId;

    @NotNull
    @NotBlank
    private String clientSecret;

    public String getAudience() {
        return audience;
    }

    public void setAudience(String audience) {
        this.audience = audience;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getClientId() {return clientId;}

    public void setClientId(String clientId) { this.clientId = clientId;}

    public String getClientSecret() { return clientSecret;}

    public void setClientSecret(String clientSecret) {this.clientSecret = clientSecret;}
}
