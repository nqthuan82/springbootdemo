package com.example.demo.configurations;

import com.example.demo.configurations.security.OAuthConfigurationProperties;
import com.example.demo.repositories.SessionRepository;
import com.example.demo.repositories.SpeakerRepository;
import com.example.demo.services.SessionsService;
import com.example.demo.services.SpeakersService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CustomConfigurations {

    @Bean
    SessionsService sessionService(SessionRepository sessionRepository){
        return new SessionsService(sessionRepository);
    }

    @Bean
    SpeakersService speakerService(SpeakerRepository speakerRepository){
        return new SpeakersService(speakerRepository);
    }
}
