package com.example.demo.controllers;

import com.example.demo.entities.Speaker;
import com.example.demo.services.SpeakersService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.security.RolesAllowed;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import static java.util.Objects.requireNonNull;

@RestController
@RequestMapping("/api/v1/speakers")
public class SpeakersController {
    private final SpeakersService speakersService;

    public SpeakersController(@NonNull SpeakersService speakersService){
        this.speakersService = requireNonNull(speakersService);
    }

    @GetMapping
    @RolesAllowed({"USER", "ADMIN"})
    public List<Speaker> list() {
        return speakersService.findAll();
    }

    @GetMapping
    @RequestMapping("{id}")
    @RolesAllowed({"USER", "ADMIN"})
    public ResponseEntity<?> get(@PathVariable Long id) throws Exception {
        Speaker speaker = speakersService.findById(id);
        return speaker == null ? ResponseEntity.notFound().build() : ResponseEntity.ok().body(speaker);
    }

    @PostMapping
    @RolesAllowed("ADMIN")
    public ResponseEntity<?> create(@RequestBody Speaker speaker){
        Speaker newSpeaker = speakersService.create(speaker);
        try{
            return ResponseEntity
                    .created(new URI("/api/v1/speakers/" + newSpeaker.getSpeakerId()))
                    .body(newSpeaker);
        }
        catch(URISyntaxException e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    @RolesAllowed("ADMIN")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Speaker speaker){
        Speaker updatedSpeaker = speakersService.update(id, speaker);
        return updatedSpeaker == null ? ResponseEntity.notFound().build() : ResponseEntity.ok().body(updatedSpeaker);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    @RolesAllowed("ADMIN")
    public ResponseEntity<?> delete(@PathVariable Long id) throws Exception{
        return speakersService.delete(id) ? ResponseEntity.ok().build() : ResponseEntity.notFound().build();
    }
}
