package com.example.demo.controllers;

import com.example.demo.entities.Session;
import com.example.demo.services.SessionsService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import static java.util.Objects.requireNonNull;

@RestController
@RequestMapping("/api/v1/sessions")
public class SessionsController {
    private static final Logger logger = LogManager.getLogger(SessionsController.class);
    private final SessionsService sessionsService;

    public SessionsController(@NonNull SessionsService sessionsService){
        this.sessionsService = requireNonNull(sessionsService);
    }

    @GetMapping
    @RolesAllowed({"USER", "ADMIN"})
    public List<Session> list(){
        return this.sessionsService.findAll();
    }

    @GetMapping
    @RequestMapping("{id}")
    @RolesAllowed({"USER", "ADMIN"})
    public ResponseEntity<?> get(@PathVariable Long id){
        Session session = sessionsService.findById(id);
        return session == null ? ResponseEntity.notFound().build() : ResponseEntity.ok().body(session);
    }

    @PostMapping
    @RolesAllowed("ADMIN")
    public ResponseEntity<?> create(@RequestBody final Session session){
        logger.info("Creating new session with name: '{}', description: '{}', length: '{}'",
                session.getSessionName(),
                session.getSessionDescription(),
                session.getSessionLength());

        Session newSession = sessionsService.create(session);
        try{
            return ResponseEntity
                    .created(new URI("/api/v1/sessions/" + newSession.getSessionId()))
                    .body(newSession);
        }
        catch(URISyntaxException e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    @RolesAllowed("ADMIN")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Session session){
        Session updatedSession = sessionsService.update(id, session);
        return updatedSession == null ? ResponseEntity.notFound().build() : ResponseEntity.ok().body(updatedSession);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    @RolesAllowed("ADMIN")
    public ResponseEntity<?> delete(@PathVariable Long id){
        return sessionsService.delete(id) ? ResponseEntity.ok().build() : ResponseEntity.notFound().build();
    }
}
