package com.example.demo.services;

import com.example.demo.entities.Session;
import com.example.demo.repositories.SessionRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.lang.NonNull;

import java.util.List;

import static java.util.Objects.requireNonNull;

public class SessionsService{

    private final SessionRepository sessionRepository;

    public SessionsService(@NonNull SessionRepository sessionRepository){
        this.sessionRepository = requireNonNull(sessionRepository);
    }

    public List<Session> findAll(){
        return this.sessionRepository.findAll();
    }

    public Session findById(Long id) {
        return sessionRepository.findById(id).orElse(null);
    }

    public Session create(Session session){
        return sessionRepository.saveAndFlush(session);
    }

    public Session update(Long id, Session session) {
        return sessionRepository.findById(id).map(s -> {
            BeanUtils.copyProperties(session, s, "sessionId");
            return sessionRepository.saveAndFlush(s);
        }).orElse(null);
    }

    public boolean delete(Long id) {
        return sessionRepository.findById(id).map(s -> {
            sessionRepository.deleteById(id);
            return true;
        }).orElse(false);
    }
}
