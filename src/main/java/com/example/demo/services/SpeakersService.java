package com.example.demo.services;

import com.example.demo.entities.Speaker;
import com.example.demo.repositories.SpeakerRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.lang.NonNull;

import java.util.List;

import static java.util.Objects.requireNonNull;

public class SpeakersService{

    private final SpeakerRepository speakerRepository;

    public SpeakersService(@NonNull SpeakerRepository speakerRepository){
        this.speakerRepository = requireNonNull(speakerRepository);
    }

    public List<Speaker> findAll(){
        return this.speakerRepository.findAll();
    }

    public Speaker findById(Long id) {
        return speakerRepository.findById(id).orElse(null);
    }

    public Speaker create(Speaker speaker){
        return speakerRepository.saveAndFlush(speaker);
    }

    public Speaker update(Long id, Speaker speaker) {
        return speakerRepository.findById(id).map(s -> {
            BeanUtils.copyProperties(speaker, s, "speakerId");
            return speakerRepository.saveAndFlush(s);
        }).orElse(null);
    }

    public boolean delete(Long id) {
        return speakerRepository.findById(id).map(s -> {
            speakerRepository.deleteById(id);
            return true;
        }).orElse(false);
    }
}
