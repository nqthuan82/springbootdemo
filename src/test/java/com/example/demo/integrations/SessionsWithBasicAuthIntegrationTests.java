package com.example.demo.integrations;

import com.example.demo.entities.Session;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.sql.DataSource;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertTrue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ActiveProfiles({"test"})
@AutoConfigureMockMvc
@Sql({"/test-data-source.sql"})
public class SessionsWithBasicAuthIntegrationTests {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private DataSource dataSource;

    @Test
    @DisplayName("Failed Login")
    public void loginInvalidUser() throws Exception {
        MvcResult result = this.mockMvc.perform(formLogin().user("invalid").password("invalid"))
                .andExpect(unauthenticated())
                .andReturn();
    }

    @Test
    @DisplayName("Get sessions list: GET /api/v1/sessions")
    public void testGetSessions() throws Exception{
        mockMvc.perform(get("/api/v1/sessions").with(httpBasic("user", "password")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(5)))
                .andExpect(jsonPath("$.*.sessionId", everyItem(allOf(notNullValue(),instanceOf(Number.class)))))
                .andExpect(jsonPath("$.*.sessionName", everyItem(allOf(notNullValue(),instanceOf(String.class)))))
                .andExpect(jsonPath("$.*.sessionDescription", everyItem(allOf(notNullValue(),instanceOf(String.class)))))
                .andExpect(jsonPath("$.*.sessionLength", everyItem(allOf(notNullValue(),instanceOf(Number.class)))));
    }

    @Test
    @DisplayName("Get session item: GET /api/v1/sessions/1 - Found")
    public void testGetSessionByIdFound() throws Exception{
        mockMvc.perform(get("/api/v1/sessions/{id}", 1).with(httpBasic("user", "password")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.sessionId", is(1)))
                .andExpect(jsonPath("$.sessionName", is("Creating Your First Data Lake in AWS")))
                .andExpect(jsonPath("$.sessionDescription", is("")))
                .andExpect(jsonPath("$.sessionLength", is(30)));
    }

    @Test
    @DisplayName("Get session item: GET /api/v1/sessions/100 - Not Found")
    public void testGetSessionByIdNotFound() throws Exception{
        mockMvc.perform(get("/api/v1/sessions/{id}", 100).with(httpBasic("user", "password")))
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("Create new session: POST /api/v1/sessions - Success")
    public void testCreateSession() throws Exception{
        Session postSession = new Session("ASP.NET CORE fundamental","",90);

        // without token with forbidden status code
        mockMvc.perform(post("/api/v1/sessions")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonStringify(postSession))
                        .with(httpBasic("user", "password")))
                .andExpect(status().isForbidden());

        mockMvc.perform(post("/api/v1/sessions")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonStringify(postSession))
                        .with(httpBasic("admin", "admin")))
                .andExpect(status().isCreated())
                .andExpect(header().string(HttpHeaders.LOCATION, "/api/v1/sessions/6"))
                .andExpect(jsonPath("$.sessionId", is(6)))
                .andExpect(jsonPath("$.sessionName", is(postSession.getSessionName())))
                .andExpect(jsonPath("$.sessionDescription", is(postSession.getSessionDescription())))
                .andExpect(jsonPath("$.sessionLength", is(postSession.getSessionLength())));

        mockMvc.perform(get("/api/v1/sessions/{id}", 6).with(httpBasic("user", "password")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.sessionId", is(6)))
                .andExpect(jsonPath("$.sessionName", is(postSession.getSessionName())))
                .andExpect(jsonPath("$.sessionDescription", is(postSession.getSessionDescription())))
                .andExpect(jsonPath("$.sessionLength", is(postSession.getSessionLength())));
    }

    @Test
    @DisplayName("Update session item: PUT /api/v1/sessions/1 - Success")
    public void testUpdateSessionSuccess() throws Exception{
        Session putSession = new Session(
                "New AWS: Create First Data Lake in AWS",
                "Update session 'Creating Your First Data Lake in AWS'",
                90);

        mockMvc.perform(put("/api/v1/sessions/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonStringify(putSession))
                        .with(httpBasic("user", "password")))
                .andExpect(status().isForbidden());

        mockMvc.perform(put("/api/v1/sessions/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonStringify(putSession))
                        .with(httpBasic("admin", "admin")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.sessionId", is(1)))
                .andExpect(jsonPath("$.sessionName", is(putSession.getSessionName())))
                .andExpect(jsonPath("$.sessionDescription", is(putSession.getSessionDescription())))
                .andExpect(jsonPath("$.sessionLength", is(putSession.getSessionLength())));

        mockMvc.perform(get("/api/v1/sessions/{id}", 1).with(httpBasic("user", "password")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.sessionId", is(1)))
                .andExpect(jsonPath("$.sessionName", is(putSession.getSessionName())))
                .andExpect(jsonPath("$.sessionDescription", is(putSession.getSessionDescription())))
                .andExpect(jsonPath("$.sessionLength", is(putSession.getSessionLength())));
    }

    @Test
    @DisplayName("Update session item: PUT /api/v1/sessions/100 - Not found")
    public void testUpdateSessionNotFound() throws Exception{
        Session putSession = new Session(
                "New AWS: Create First Data Lake in AWS",
                "Update session 'Creating Your First Data Lake in AWS'",
                90);
        mockMvc.perform(put("/api/v1/sessions/{id}", 100)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonStringify(putSession))
                        .with(httpBasic("user", "password")))
                .andExpect(status().isForbidden());

        mockMvc.perform(put("/api/v1/sessions/{id}", 100)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonStringify(putSession))
                        .with(httpBasic("admin", "admin")))
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("Delete session item: DELETE /api/v1/sessions/1")
    public void testDeleteSessionSuccess() throws Exception{
        mockMvc.perform(delete("/api/v1/sessions/1").with(httpBasic("admin", "admin")))
                .andExpect(status().isOk());

        mockMvc.perform(delete("/api/v1/sessions/1").with(httpBasic("admin", "admin")))
                .andExpect(status().isNotFound());

        mockMvc.perform(delete("/api/v1/sessions/1").with(httpBasic("user", "password")))
                .andExpect(status().isForbidden());
    }

    static String jsonStringify(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
