package com.example.demo.services;

import com.example.demo.entities.Speaker;
import com.example.demo.repositories.SpeakerRepository;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.lenient;

@RunWith(MockitoJUnitRunner.class)
public class SpeakerServiceTests {
    @Mock
    private SpeakerRepository repository;

    @InjectMocks
    private SpeakersService speakerService;

    @Test
    public void testFindByIdSuccess(){
        Speaker mockSpeaker = new Speaker(1L,"Sergio","Becker","Senior Developer","MicroOcean Software","Test", null);
        doReturn(Optional.of(mockSpeaker)).when(repository).findById(1L);

        Speaker returnedSpeaker = speakerService.findById(1L);

        Assertions.assertNotNull(returnedSpeaker, "Speaker was not found");
        Assertions.assertSame(mockSpeaker, returnedSpeaker, "Speakers should be the same");
    }

    @Test
    public void testFindByIdNotFound(){
        doReturn(Optional.empty()).when(repository).findById(1L);
        Speaker returnedSession = speakerService.findById(1L);
        Assertions.assertNull(returnedSession, "Speaker should be null");
    }

    @Test
    public void createSessionSuccess(){
        Speaker mockSpeaker = new Speaker(1L,"Sergio","Becker","Senior Developer","MicroOcean Software","Test", null);
        doReturn(mockSpeaker).when(repository).saveAndFlush(any());

        Speaker newSession = speakerService.create(mockSpeaker);
        Assertions.assertSame(mockSpeaker, newSession, "Speakers should be the same");
    }

    @Test
    public void updateSessionSuccess(){
        Speaker mockSpeaker = new Speaker(1L,"Sergio","Becker","Senior Developer","MicroOcean Software","Test", null);
        doReturn(Optional.of(mockSpeaker)).when(repository).findById(1L);
        doReturn(mockSpeaker).when(repository).saveAndFlush(any());

        Speaker updatedSession = speakerService.update(1L,mockSpeaker);
        Assertions.assertSame(mockSpeaker, updatedSession, "Speakers should be the same");
    }

    @Test
    public void updateSessionNotFound(){
        Speaker mockSpeaker = new Speaker(1L,"Sergio","Becker","Senior Developer","MicroOcean Software","Test", null);
        doReturn(Optional.empty()).when(repository).findById(1L);
        lenient().doReturn(mockSpeaker).when(repository).saveAndFlush(any());

        Speaker updatedSession = speakerService.update(1L,mockSpeaker);
        Assertions.assertNull(updatedSession, "Speakers should be null");
    }

    @Test
    public void deleteSessionSuccess(){
        Speaker mockSpeaker = new Speaker(1L,"Sergio","Becker","Senior Developer","MicroOcean Software","Test", null);
        doReturn(Optional.of(mockSpeaker)).when(repository).findById(1L);
        doNothing().when(repository).deleteById(1L);

        boolean isDeleted = speakerService.delete(1L);
        Assertions.assertTrue(isDeleted, "Speakers should be the deleted");
    }

    @Test
    public void deleteSessionNotFound(){
        Speaker mockSpeaker = new Speaker(1L,"Sergio","Becker","Senior Developer","MicroOcean Software","Test", null);
        doReturn(Optional.empty()).when(repository).findById(1L);
        lenient().doNothing().when(repository).deleteById(1L);

        boolean isDeleted = speakerService.delete(1L);
        Assertions.assertFalse(isDeleted, "Speakers should not be found");
    }

    @Test
    public void testFindAll(){
        List<Speaker> mockSpeakers = Arrays.asList(
                new Speaker(1L,"Sergio","Becker","Senior Developer","MicroOcean Software","Test", null),
                new Speaker(2L,"James","Lowrey","Solutions Architect","Fabrikam Industries","Test", null)
        );
        doReturn(mockSpeakers).when(repository).findAll();
        List<Speaker> returnedSessions = speakerService.findAll();
        Assertions.assertEquals(2, returnedSessions.size(), "findAll should returned 3 speakers");
        Assertions.assertEquals(mockSpeakers, returnedSessions, "findAll should return the same speakers list");
    }
}
