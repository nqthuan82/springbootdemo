package com.example.demo.services;

import com.example.demo.entities.Session;
import com.example.demo.repositories.SessionRepository;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.lenient;

@RunWith(MockitoJUnitRunner.class)
public class SessionsServiceTests {

    @Mock
    private SessionRepository repository;

    @InjectMocks
    private SessionsService sessionService;

    @Test
    public void testFindByIdSuccess(){
        Session mockSession = new Session(1L,"Keynote - The Golden Age of Software","",45);
        doReturn(Optional.of(mockSession)).when(repository).findById(1L);

        Session returnedSession = sessionService.findById(1L);

        Assertions.assertNotNull(returnedSession, "Session was not found");
        Assertions.assertSame(mockSession, returnedSession, "Sessions should be the same");
    }

    @Test
    public void testFindByIdNotFound(){
        doReturn(Optional.empty()).when(repository).findById(1L);
        Session returnedSession = sessionService.findById(1L);
        Assertions.assertNull(returnedSession, "Session should be null");
    }

    @Test
    public void createSessionSuccess(){
        Session mockSession = new Session(1L,"Keynote - The Golden Age of Software","",45);
        doReturn(mockSession).when(repository).saveAndFlush(any());

        Session newSession = sessionService.create(mockSession);
        Assertions.assertSame(mockSession, newSession, "Sessions should be the same");
    }

    @Test
    public void updateSessionSuccess(){
        Session mockSession = new Session(1L,"Keynote - The Golden Age of Software","",45);
        doReturn(Optional.of(mockSession)).when(repository).findById(1L);
        doReturn(mockSession).when(repository).saveAndFlush(any());

        Session updatedSession = sessionService.update(1L,mockSession);
        Assertions.assertSame(mockSession, updatedSession, "Sessions should be the same");
    }

    @Test
    public void updateSessionNotFound(){
        Session mockSession = new Session(1L,"Keynote - The Golden Age of Software","",45);
        doReturn(Optional.empty()).when(repository).findById(1L);
        lenient().doReturn(mockSession).when(repository).saveAndFlush(any());

        Session updatedSession = sessionService.update(1L,mockSession);
        Assertions.assertNull(updatedSession, "Sessions should be null");
    }

    @Test
    public void deleteSessionSuccess(){
        Session mockSession = new Session(1L,"Keynote - The Golden Age of Software","",45);
        doReturn(Optional.of(mockSession)).when(repository).findById(1L);
        doNothing().when(repository).deleteById(1L);

        boolean isDeleted = sessionService.delete(1L);
        Assertions.assertTrue(isDeleted, "Sessions should be the deleted");
    }

    @Test
    public void deleteSessionNotFound(){
        Session mockSession = new Session(1L,"Keynote - The Golden Age of Software","",45);
        doReturn(Optional.empty()).when(repository).findById(1L);
        lenient().doNothing().when(repository).deleteById(1L);

        boolean isDeleted = sessionService.delete(1L);
        Assertions.assertFalse(isDeleted, "Sessions should not be found");
    }

    @Test
    public void testFindAll(){
        List<Session> mockSessions = Arrays.asList(
                new Session(1L,"Keynote - The Golden Age of Software","",45),
                new Session(2L,"A Better Way to Access Data with Spring Data","",60),
                new Session(3L,"A Deep Dive Into Spring IoC","",60)
        );
        doReturn(mockSessions).when(repository).findAll();
        List<Session> returnedSessions = sessionService.findAll();
        Assertions.assertEquals(3, returnedSessions.size(), "findAll should returned 3 sessions");
        Assertions.assertEquals(mockSessions, returnedSessions, "findAll should return the same sessions list");
    }
}
