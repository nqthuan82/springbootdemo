package com.example.demo.controllers;

import com.example.demo.entities.Session;
import com.example.demo.services.SessionsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(SessionsController.class)
//@AutoConfigureMockMvc(addFilters = false) // Remove filters (authenticate/authorize ...) from MockMvc
public class SessionsControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SessionsService service;

    @Test
    @DisplayName("GET /api/v1/sessions/1 - Found")
    @WithMockUser(roles = "USER")
    public void testGetSessionByIdFound() throws Exception{
        Session mockSession = new Session(1L,"Keynote - The Golden Age of Software","",45);
        doReturn(mockSession).when(service).findById(1L);

        mockMvc.perform(get("/api/v1/sessions/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.sessionId", is(1)))
                .andExpect(jsonPath("$.sessionName", is("Keynote - The Golden Age of Software")))
                .andExpect(jsonPath("$.sessionDescription", is("")))
                .andExpect(jsonPath("$.sessionLength", is(45)));
    }

    @Test
    @DisplayName("GET /api/v1/sessions/1 - Forbidden ")
    @WithMockUser(roles = "invalidRole")
    public void testGetSessionByIdForbidden() throws Exception{
        Session mockSession = new Session(1L,"Keynote - The Golden Age of Software","",45);
        doReturn(mockSession).when(service).findById(1L);

        mockMvc.perform(get("/api/v1/sessions/{id}", 1))
                .andExpect(status().isForbidden());
    }

    @Test
    @DisplayName("GET /api/v1/sessions/1 - Not Found")
    @WithMockUser(roles = "USER")
    public void testGetSessionByIdNotFound() throws Exception{
        doReturn(null).when(service).findById(1L);

        mockMvc.perform(get("/api/v1/sessions/{id}", 1))
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("POST /api/v1/sessions - Success")
    @WithMockUser(roles = "ADMIN")
    public void testCreateSession() throws Exception{
        Session postSession = new Session("Keynote - The Golden Age of Software","",45);
        Session mockSession = new Session(1L,"Keynote - The Golden Age of Software","",45);
        doReturn(mockSession).when(service).create(any());
        mockMvc.perform(post("/api/v1/sessions")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonStringify(postSession)))
                .andExpect(status().isCreated())
                .andExpect(header().string(HttpHeaders.LOCATION, "/api/v1/sessions/1"))
                .andExpect(jsonPath("$.sessionId", is(1)))
                .andExpect(jsonPath("$.sessionName", is("Keynote - The Golden Age of Software")))
                .andExpect(jsonPath("$.sessionDescription", is("")))
                .andExpect(jsonPath("$.sessionLength", is(45)));
    }

    @Test
    @DisplayName("PUT /api/v1/sessions/1 - Success")
    @WithMockUser(roles = "ADMIN")
    public void testUpdateSessionSuccess() throws Exception{
        Session putSession = new Session("Updated - Keynote - The Golden Age of Software","",45);
        Session mockSession = new Session(1L,"Updated - Keynote - The Golden Age of Software","",45);
        doReturn(mockSession).when(service).update(any(), any());
        mockMvc.perform(put("/api/v1/sessions/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonStringify(putSession)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.sessionId", is(1)))
                .andExpect(jsonPath("$.sessionName", is(putSession.getSessionName())))
                .andExpect(jsonPath("$.sessionDescription", is(putSession.getSessionDescription())))
                .andExpect(jsonPath("$.sessionLength", is(putSession.getSessionLength())));
    }

    @Test
    @DisplayName("PUT /api/v1/sessions/1 - Not found")
    @WithMockUser(roles = "ADMIN")
    public void testUpdateSessionNotFound() throws Exception{
        Session putSession = new Session("Updated - Keynote - The Golden Age of Software","",45);
        doReturn(null).when(service).update(any(), any());
        mockMvc.perform(put("/api/v1/sessions/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonStringify(putSession)))
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("DELETE /api/v1/sessions/1 - success")
    @WithMockUser(roles = "ADMIN")
    public void testDeleteSessionSuccess() throws Exception{
        Session mockSession = new Session(1L,"Keynote - The Golden Age of Software","",45);
        doReturn(true).when(service).delete(1L);
        mockMvc.perform(delete("/api/v1/sessions/1"))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("DELETE /api/v1/sessions/1 - Not found")
    @WithMockUser(roles = "ADMIN")
    public void testDeleteSessionNotFound() throws Exception{
        doReturn(false).when(service).delete(1L);
        mockMvc.perform(delete("/api/v1/sessions/1"))
                .andExpect(status().isNotFound());
    }


    @Test
    @DisplayName("GET /api/v1/sessions")
    @WithMockUser(roles = "USER")
    public void testGetSessions() throws Exception{
        List<Session> sessions = Arrays.asList(
                new Session(1L,"Keynote - The Golden Age of Software","",45),
                new Session(2L,"A Better Way to Access Data with Spring Data","",60),
                new Session(3L,"A Deep Dive Into Spring IoC","",60)
        );
        doReturn(sessions).when(service).findAll();
        mockMvc.perform(get("/api/v1/sessions", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$.*.sessionId", everyItem(allOf(notNullValue(),instanceOf(Number.class)))))
                .andExpect(jsonPath("$.*.sessionName", everyItem(allOf(notNullValue(),instanceOf(String.class)))))
                .andExpect(jsonPath("$.*.sessionDescription", everyItem(allOf(notNullValue(),instanceOf(String.class)))))
                .andExpect(jsonPath("$.*.sessionLength", everyItem(allOf(notNullValue(),instanceOf(Number.class)))));
    }

    static String jsonStringify(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

