package com.example.demo.controllers;
import com.example.demo.entities.Speaker;
import com.example.demo.services.SpeakersService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(SpeakersController.class)
//@AutoConfigureMockMvc(addFilters = false)
public class SpeakersControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SpeakersService service;

    @Test
    @DisplayName("GET /api/v1/speakers/1 - Found")
    @WithMockUser(roles = "USER")
    public void testGetSpeakerByIdFound() throws Exception{
        Speaker mockSpeaker = new Speaker(1L,"Sergio","Becker","Senior Developer","MicroOcean Software","Test", null);
        doReturn(mockSpeaker).when(service).findById(1L);

        mockMvc.perform(get("/api/v1/speakers/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.speakerId", is(1)))
                .andExpect(jsonPath("$.firstName", is(mockSpeaker.getFirstName())))
                .andExpect(jsonPath("$.lastName", is(mockSpeaker.getLastName())))
                .andExpect(jsonPath("$.title", is(mockSpeaker.getTitle())))
                .andExpect(jsonPath("$.company", is(mockSpeaker.getCompany())))
                .andExpect(jsonPath("$.speakerBio", is(mockSpeaker.getSpeakerBio())))
                .andExpect(jsonPath("$.speakerPhoto", is(mockSpeaker.getSpeakerPhoto())));
    }

    @Test
    @DisplayName("GET /api/v1/speakers/1 - Forbidden")
    @WithMockUser(roles = "invalidRole")
    public void testGetSpeakerByIdForbidden() throws Exception{
        Speaker mockSpeaker = new Speaker(1L,"Sergio","Becker","Senior Developer","MicroOcean Software","Test", null);
        doReturn(mockSpeaker).when(service).findById(1L);

        mockMvc.perform(get("/api/v1/speakers/{id}", 1))
                .andExpect(status().isForbidden());
    }

    @Test
    @DisplayName("GET /api/v1/speakers/1 - Not Found")
    @WithMockUser(roles = "USER")
    public void testGetSpeakerByIdNotFound() throws Exception{
        doReturn(null).when(service).findById(1L);

        mockMvc.perform(get("/api/v1/speakers/{id}", 1))
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("POST /api/v1/speakers - Success")
    @WithMockUser(roles = "ADMIN")
    public void testCreateSpeaker() throws Exception{
        Speaker postSpeaker = new Speaker("Sergio","Becker","Senior Developer","MicroOcean Software","Test", null);
        Speaker mockSpeaker = new Speaker(1L,"Sergio","Becker","Senior Developer","MicroOcean Software","Test", null);
        doReturn(mockSpeaker).when(service).create(any());
        mockMvc.perform(post("/api/v1/speakers")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonStringify(postSpeaker)))
                .andExpect(status().isCreated())
                .andExpect(header().string(HttpHeaders.LOCATION, "/api/v1/speakers/1"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.speakerId", is(1)))
                .andExpect(jsonPath("$.firstName", is(mockSpeaker.getFirstName())))
                .andExpect(jsonPath("$.lastName", is(mockSpeaker.getLastName())))
                .andExpect(jsonPath("$.title", is(mockSpeaker.getTitle())))
                .andExpect(jsonPath("$.company", is(mockSpeaker.getCompany())))
                .andExpect(jsonPath("$.speakerBio", is(mockSpeaker.getSpeakerBio())))
                .andExpect(jsonPath("$.speakerPhoto", is(mockSpeaker.getSpeakerPhoto())));
    }

    @Test
    @DisplayName("PUT /api/v1/speakers/1 - Success")
    @WithMockUser(roles = "ADMIN")
    public void testUpdateSpeakerSuccess() throws Exception{
        Speaker putSpeaker = new Speaker("Sergio","Becker","Senior Developer","MicroOcean Software","Test", null);
        Speaker mockSpeaker = new Speaker(1L,"Sergio","Becker","Senior Developer","MicroOcean Software","Test", null);
        doReturn(mockSpeaker).when(service).update(any(), any());
        mockMvc.perform(put("/api/v1/speakers/{id}",1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonStringify(putSpeaker)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.speakerId", is(1)))
                .andExpect(jsonPath("$.firstName", is(mockSpeaker.getFirstName())))
                .andExpect(jsonPath("$.lastName", is(mockSpeaker.getLastName())))
                .andExpect(jsonPath("$.title", is(mockSpeaker.getTitle())))
                .andExpect(jsonPath("$.company", is(mockSpeaker.getCompany())))
                .andExpect(jsonPath("$.speakerBio", is(mockSpeaker.getSpeakerBio())))
                .andExpect(jsonPath("$.speakerPhoto", is(mockSpeaker.getSpeakerPhoto())));
    }

    @Test
    @DisplayName("PUT /api/v1/speakers/1 - Not found")
    @WithMockUser(roles = "ADMIN")
    public void testUpdateSpeakerNotFound() throws Exception{
        Speaker putSpeaker = new Speaker("Sergio","Becker","Senior Developer","MicroOcean Software","Test", null);
        doReturn(null).when(service).update(any(), any());
        mockMvc.perform(put("/api/v1/speakers/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonStringify(putSpeaker)))
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("DELETE /api/v1/speakers/1 - success")
    @WithMockUser(roles = "ADMIN")
    public void testDeleteSpeakerSuccess() throws Exception{
        Speaker mockSpeaker = new Speaker(1L,"Sergio","Becker","Senior Developer","MicroOcean Software","Test", null);
        doReturn(true).when(service).delete(1L);
        mockMvc.perform(delete("/api/v1/speakers/1"))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("DELETE /api/v1/speakers/1 - Not found")
    @WithMockUser(roles = "ADMIN")
    public void testDeleteSpeakerNotFound() throws Exception{
        doReturn(false).when(service).delete(1L);
        mockMvc.perform(delete("/api/v1/speakers/1"))
                .andExpect(status().isNotFound());
    }


    @Test
    @DisplayName("GET /api/v1/speakers")
    @WithMockUser(roles = "ADMIN")
    public void testGetSpeakers() throws Exception{
        List<Speaker> speakers = Arrays.asList(
                new Speaker(1L,"Sergio","Becker","Senior Developer","MicroOcean Software","Test", null),
                new Speaker(2L,"James","Lowrey","Solutions Architect","Fabrikam Industries","Test", null)
        );
        doReturn(speakers).when(service).findAll();
        mockMvc.perform(get("/api/v1/speakers", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$.*.speakerId", everyItem(allOf(notNullValue(),instanceOf(Number.class)))))
                .andExpect(jsonPath("$.*.firstName", everyItem(allOf(notNullValue(),instanceOf(String.class)))))
                .andExpect(jsonPath("$.*.lastName", everyItem(allOf(notNullValue(),instanceOf(String.class)))))
                .andExpect(jsonPath("$.*.title", everyItem(allOf(notNullValue(),instanceOf(String.class)))))
                .andExpect(jsonPath("$.*.company", everyItem(allOf(notNullValue(),instanceOf(String.class)))))
                .andExpect(jsonPath("$.*.speakerBio", everyItem(allOf(notNullValue(),instanceOf(String.class)))))
                .andExpect(jsonPath("$.*.speakerPhoto", everyItem(anyOf(nullValue(), instanceOf(byte[].class)))));
    }

    static String jsonStringify(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
