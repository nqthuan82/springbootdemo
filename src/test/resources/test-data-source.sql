DELETE FROM session_speakers;
DELETE FROM sessions;
DELETE FROM speakers;

INSERT INTO sessions (session_id,session_name,session_length,session_description)
VALUES
       (1,'Creating Your First Data Lake in AWS',30,''),
       (2,'Migrating to Amazon Aurora',30,''),
       (3,'How Agile Are You Really?',60,''),
       (4,'Better Retrospectives',60,''),
       (5,'Developer to Leader',60,'');

INSERT INTO speakers (speaker_id,first_name,last_name,title,company,speaker_bio,speaker_photo)
VALUES (1,'Cynthia','Crandall','Senior Business Analyst','Wired Brain Coffee','Test', null),
       (2,'Clara','Dawson','Agile Coach','Agile Coaches Inc','Test', null),
       (3,'Ann','Martinez','Senior AWS Consultant','Western Consulting Services','Test', null),
       (4,'James','King','Staff AWS Engineer','Northern States Bank','Test', null),
       (5,'Simon','Williams','Chief Technology Officer','NorthernSoft Systems','Test', null);

INSERT INTO session_speakers (session_id,speaker_id)
VALUES (1,1),
       (2,1),
       (3,2),
       (4,3),
       (5,4),
       (5,5);

ALTER TABLE sessions  ALTER COLUMN session_id RESTART WITH 6;
ALTER TABLE speakers  ALTER COLUMN speaker_id RESTART WITH 6;
